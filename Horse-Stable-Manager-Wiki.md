# Horse Stable Manager

[scroll down for 'quick start' instructions]

Horse stable manager is a turn-based meta-data text based sports sim that lets you over-dose on numbers and graphs on horses and races so you can manager your racing stable and become increasingly successful.

You can play single player and strive on your own or play with as many off your mates as you like. One person is always the host and determines the speed of each turn.

## Play-by-email

Play-by-email (PBEM) typically involves the players to exchange game files with each other but it doesn't need to be email. Discord and other chat programs can let you share files with players in real time.

Players in Horse Stable Manager will view the game files and choose what actions to take. Some examples:

- by a horse
- train a horse
- enter a horse into a race
- place a bet on a horse

The key thing with PBEM is that the actions you take are not executed immediately. Your actions are added to a queue and sent to the game host. This is the PBEM bit - you send your actions to the host by sending a file to the host. The host will also receive actions from all the other players.

When the host has received intended actions from all the players then the host can use a special host mode to "crunch" all the actions and determine results and outcomes. The host will then distribute fresh game files back to every player so they can review results, determine their successful actions and then plan the next game loop.

### Instructions

#### How to get started

##### Host

All players (and host) needs to download and install the game. The host will then create a new save game and share the game files with all the players. I suggest something like DISCORD is used to share files quickly with a drag/drop option.

Open up the game folder where the game is installed and navigate to the "\savedata" folder. The host will see their newly created save game here (example: \savedata\mygame). Zip this file (eg: mygame.zip) and share it with all the players.

##### Players

Unzip the file the host shares with you (eg: mygame.zip) into the "\savedata" folder. If you do this right, you'll have the following folder structure (example):
\savedata\mygame\mygame_sharedfiles
and you'll have a bunch of files in the "_sharedfiles" folder.

You are now ready to play! Open the game, select your save game ("mygame") and then click the "Player" button because you want to be a player.

Note: you can play single-player by being the player and the host.

#### Game loop

The game loop always follows the same pattern:

- players use the "Players" option in the game to view all the meta-data and make decisions on what to do next. As they click buttons and make decisions, their actions are queued and saved on file. Nothing will happen immediately but they will see their list of actions grow.

- when players are ready they send their actions file to the host. This is located in:
"\savedata\mygame\mygame_sharedfiles\actions_xyz.dat"

The player needs to take care to only send the actions file. The host needs to take care to only accept the actions file.

- the host needs to receive all the action files from every player and save them into "\savedata\mygame\mygame_sharedfiles". Each action file should already have a unique name so just save them all into that folder. Collecting all the action files might take an hour, a day or a week. As the host, you should get the players agreement on how often they are to send files.

- when its time to end the turn, the host chooses the "Host" option in the game. The host will see all the actions queued up for all the players. The host can then process all the actoins by clicking the correct button.

- at this point, the host has two options:
1) return the game files to the players so they can see the other players options (see instructions below)
2) end the turn with the correct button

Returning the updated game files to the players will significantly slow the game down but it gives players the ability to see what other players and bots are doing and to then make more actions. For example, Player A may see Player B enter a horse into a race. Player A might want the option to place a bet on that horse.

Option 2 is recommended as it sets a faster pace for the game.

- Return the updated game files to the players. Zip up the whole "\savedata\mygame\mygame_sharedfiles" folder and send that to every player. This will allow the players to see the outcomes of their own actions and the actions of other players

- go to top of the loop and repeat.  :)

#### Important notes

- the players should only ever send their actions_.dat file. Never ever more than that.
- the host should only ever accept the players actions_.dat file. Reject all the other files
- the host needs to share the entire "_sharedfiles" folder. That's why it is called 'shared files'.  ;)
- the player needs to unzip the whole "_sharedfiles" folder into the right place and overwrite existing files.

#### Tips

- agree up front how often the host will process and distribute game files. It can be a few times a week, once a week or maybe once a month. Decide what is most comfortable.
- run multiple save games on different schedules with different players.
- play by yourself by hosting your own game. You won't need to transfer files if you are player and host.
- use a social media app like Discord to exchange files and have some fun banter at the same time.
- players can join at any point. Simply have the host send them the shared files and they can join the fun
- a host can end the turn without receiving all the player actions files. If a player is absent then previously queued actions will still be effective.
- a player that knows they will be absent (e.g. work or holiday) can queue actions in advance and let the host know they player expects to skip some turns. The actions will still be processed and the player can rejoin the fun when they return.

## Quck-start for single-players

Here is how a single player can get started quickly and understand the game loop.

### Create a stable/company

For the moment, the game will assign a stable name to every new player.

- start the game so you see the main menu
- type a new campaign name into the text box at the bottom of the main menu (eg "my first campaign")
- click "start new campaign"

Your save game "slot" will appear at the top of the main menu

- click your newly created save game. It will turn green.
- click "Player" button because, for now, you're a player that wants to create a stable.

The main game screen will display. Here you can see the race schedule on the left, your stable name and funds at the top and some system messages on the bottom. Most boxes will be empty because the campaign has just started.

### Buy a horse

- click the "View all horses" button in the bottom right corner. All horses in the game world are on display
- use the mouse wheel to scroll through the list of horses
- click on a horse you can afford
- click "Buy horse". You will see a message in the ACTIONS box. You have made a request to purchase that horse
- click "Back" two times to return to the main menu

### Switch to HOST mode

As a player, you have requested the creation of a new stable and you have requested the purchase of a horse. These two things don't become a part of the game world until the host processes your requests. Since this is a single player game you are the player and the host but you use two different screens.

- ensure you are on the main menu and your game slot is green
- click the "Host" button

The host will see the requested actions in the ACTIONS box. These are your own requests to create a stable and purchase a horse.

- click "Process actions" two times. You will see the creation of system messages as the actions are processed.

Your stable and horse is now created into the game world.

### Process the racing month

The "turn" is over when the host has processed all actions and then "ends" the month.

- click "Process month". More system messages are shown to the host.

The host has processed the month and now we switch back to player-mode.

- click "Back" to return to the main menu

### Player mode

- ensure your game slot is green and then click "Player"

You can now see you have less money to spend and you have one owned horse. Lets enter your horse into a race.

- click your horse so it is highlighted
- find a race on the left of the screen that your horse is qualified to race in
- click that race
- click "Enter horse in race". An action will appear at the bottom of the screen

Horses can't run in every race. You'll need to check if your horse can enter a "5f", "6f" or "10f" race. You'll also need to look at the number of wins your horse has. Don't try to enter your horse into a "1 win" race until your horse has 1 win on it's record.

Remember - the action below shows you have requested the horse be entered into the race. The game world won't be updated until the host processes your request.

### Switch to host mode (again)

Switch back to host mode, process all actions, then process the month and repeat.  :)

See if you can make money by strategically entering races, training horses and placing careful bets on horses.












