Play-by-email (PBEM) typically involves the players to exchange game files with each other but it doesn't need to be email. Discord and other chat programs can let you share files with players in real time.

Players in Horse Stable Manager will view the game files and choose what actions to take. Some examples:

- by a horse
- train a horse
- enter a horse into a race
- place a bet on a horse

The key thing with PBEM is that the actions you take are not executed immediately. Your actions are added to a queue and sent to the game host. This is the PBEM bit - you send your actions to the host by sending a file to the host. The host will also receive actions from all the other players.

When the host has received intended actions from all the players then the host can use a special host mode to "crunch" all the actions and determine results and outcomes. The host will then distribute fresh game files back to every player so they can review results, determine their successful actions and then plan the next game loop.